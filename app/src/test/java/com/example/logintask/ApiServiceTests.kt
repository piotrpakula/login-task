package com.example.logintask

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.logintask.api.ApiEndpoints
import com.example.logintask.api.models.LoginRequestModel
import com.example.logintask.api.models.LoginResponseModel
import com.google.gson.Gson
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@RunWith(JUnit4::class)
class ApiServiceTests {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var apiService: ApiEndpoints
    private lateinit var mockWebServer: MockWebServer

    @Before
    fun createWebServer() {
        mockWebServer = MockWebServer()
        apiService = Retrofit.Builder()
            .baseUrl(mockWebServer.url(""))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiEndpoints::class.java)
    }

    @After
    fun stopService() {
        mockWebServer.shutdown()
    }

    @Test
    fun `Should return token when user login with correct data`() = runBlocking {
        mockWebServer.enqueue(MockResponse().setBody(Gson().toJson(loginResponseModel)))

        val resultResponse = apiService.login(loginRequestModel).body()

        val request = mockWebServer.takeRequest()
        assertNotNull(resultResponse)
        assertThat(request.path, CoreMatchers.`is`("/api/v1/login"))
        assertEquals(loginResponseModel.token, resultResponse?.token)
    }

    companion object {
        val loginRequestModel = LoginRequestModel("email@gmail.com", "password")
        val loginResponseModel = LoginResponseModel("200 OK", "bearer@Token")
    }
}