package com.example.logintask

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.logintask.api.models.LoginRequestModel
import com.example.logintask.api.models.LoginResponseModel
import com.example.logintask.main.login.LoginViewModel
import com.example.logintask.main.login.models.LoginValidationError.*
import com.example.logintask.repositories.Repository
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import okhttp3.MediaType.get
import okhttp3.ResponseBody
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import pl.piotr.artistinfoapp.utilities.getValue
import retrofit2.Response

class LoginViewModelTests {

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")
    private val repository: Repository = mock()

    private lateinit var viewModel: LoginViewModel

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun before() = runBlocking {
        Dispatchers.setMain(mainThreadSurrogate)
        viewModel = LoginViewModel(repository)
    }

    @After
    fun after() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `Should return token when user login with correct data`() = runBlocking {
        viewModel.email.value = loginRequestModel.email
        viewModel.password.value = loginRequestModel.password
        whenever(repository.login(loginRequestModel))
            .doReturn(Response.success(loginResponseModel))

        viewModel.login()

        assertEquals(loginResponseModel, getValue(viewModel.response).body())
        assertEquals(200, getValue(viewModel.response).code())
    }

    @Test
    fun `Should throw no values validation errors when user login with missing data`() =
        runBlocking {
            viewModel.login()

            assertEquals(NoValue, getValue(viewModel.emailValidationError))
            assertFalse(getValue(viewModel.isEmailValid))
            assertEquals(NoValue, getValue(viewModel.passwordValidationError))
            assertFalse(getValue(viewModel.isPasswordValid))
        }

    @Test
    fun `Should throw wrong format validation error when user login with wrong format email`() =
        runBlocking {
            viewModel.email.value = "wrongFormatEmail"

            viewModel.login()

            assertEquals(WrongEmailFormat, getValue(viewModel.emailValidationError))
            assertFalse(getValue(viewModel.isEmailValid))
        }

    @Test
    fun `Should throw unauthorized error when user login with wrong data`() = runBlocking {
        viewModel.email.value = loginRequestModel.email
        viewModel.password.value = loginRequestModel.password
        whenever(repository.login(loginRequestModel))
            .doReturn(Response.error(401, ResponseBody.create(get("application/json"), "{}")))

        viewModel.login()

        assertEquals(401, getValue(viewModel.response).code())
    }

    @Test
    fun `Should throw server internal error when server is off while login`() = runBlocking {
        viewModel.email.value = loginRequestModel.email
        viewModel.password.value = loginRequestModel.password
        whenever(repository.login(loginRequestModel))
            .doReturn(Response.error(500, ResponseBody.create(get("application/json"), "{}")))

        viewModel.login()

        assertEquals(500, getValue(viewModel.response).code())
    }

    companion object {
        val loginRequestModel = LoginRequestModel("email@gmail.com", "pass")
        val loginResponseModel = LoginResponseModel("200 OK", "bearerToken")
    }
}