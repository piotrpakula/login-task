package com.example.logintask

import com.example.logintask.tools.ValidatorHelper.validateEmail
import junit.framework.Assert.assertFalse
import junit.framework.Assert.assertTrue
import org.junit.Test

class ValidatorHelperTests {

    @Test
    fun `Should return true when email is correct`() {
        assertTrue(validateEmail(correctEmail))
    }

    @Test
    fun `Should return false when email is empty`() {
        assertFalse(validateEmail(emptyEmail))
    }

    @Test
    fun `Should return false when email has not correct format`() {
        assertFalse(validateEmail(brokenEmail))
    }

    companion object {
        const val correctEmail = "email@gmail.com"
        const val emptyEmail = ""
        const val brokenEmail = "brokenEmail.com"
    }
}