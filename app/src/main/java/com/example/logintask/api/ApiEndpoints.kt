package com.example.logintask.api

import com.example.logintask.api.models.LoginRequestModel
import com.example.logintask.api.models.LoginResponseModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiEndpoints {

    @POST("/api/v1/login")
    suspend fun login(@Body loginRequestModel: LoginRequestModel): Response<LoginResponseModel>
}