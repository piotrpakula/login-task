package com.example.logintask.api.models

data class LoginResponseModel(
    val status: String,
    val token: String
)