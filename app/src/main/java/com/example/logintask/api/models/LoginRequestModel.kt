package com.example.logintask.api.models

data class LoginRequestModel(
    val email: String,
    val password: String
)