package com.example.logintask.repositories

import com.example.logintask.api.ApiEndpoints
import com.example.logintask.api.models.LoginRequestModel
import com.example.logintask.api.models.LoginResponseModel
import retrofit2.Response
import javax.inject.Inject

class Repository @Inject constructor(
    private val apiEndpoints: ApiEndpoints
) {
    suspend fun login(loginRequestModel: LoginRequestModel): Response<LoginResponseModel> {
        return apiEndpoints.login(loginRequestModel)
    }
}