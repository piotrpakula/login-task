package com.example.logintask.di

import android.app.Application
import com.example.logintask.GlobalApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import com.example.logintask.di.modules.ViewModelModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ApiModule::class,
        MainActivityModule::class,
        ViewModelModule::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(application: GlobalApplication)
}