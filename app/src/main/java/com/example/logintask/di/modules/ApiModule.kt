package com.example.logintask.di

import com.example.logintask.api.ApiEndpoints
import com.example.logintask.api.ApiService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class ApiModule {

    @Singleton
    @Provides
    fun provideRetrofit() = ApiService().retrofit

    @Singleton
    @Provides
    fun provideApiEndpoints(retrofit: Retrofit) = retrofit.create(ApiEndpoints::class.java)
}