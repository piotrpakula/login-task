package com.example.logintask.di

import com.example.logintask.main.login.LoginFragment
import com.example.logintask.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributeFragmentLogin(): LoginFragment
}