package com.example.logintask

import android.app.Activity
import android.app.Application
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import com.example.logintask.di.AppInjector
import javax.inject.Inject

class GlobalApplication : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()

        AppInjector.initialize(this)
    }

    override fun activityInjector() = dispatchingAndroidInjector
}