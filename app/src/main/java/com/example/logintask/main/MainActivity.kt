package com.example.logintask.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil.setContentView
import androidx.fragment.app.Fragment
import com.example.logintask.R
import com.example.logintask.databinding.ActivityMainBinding
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    private var doubleBackPressed = false
    private var doubleBackPressedJob: Job? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView<ActivityMainBinding>(this, R.layout.activity_main)
    }

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    override fun onBackPressed() {
        if (doubleBackPressed) {
            finish()
            return
        }
        doubleBackPressed = true
        doubleBackPressedJob = GlobalScope.launch {
            delay(500)
            doubleBackPressed = false
            if (!isDestroyed) runOnUiThread { super.onBackPressed() }
        }
    }

    override fun onPause() {
        doubleBackPressedJob?.cancel()
        super.onPause()
    }
}
