package com.example.logintask.main.login.models

import androidx.annotation.StringRes
import com.example.logintask.R

enum class LoginValidationError(@StringRes val text : Int){
    NoValue(R.string.no_value_error),
    WrongEmailFormat(R.string.wrong_format_error)
}