package com.example.logintask.main.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.logintask.api.models.LoginRequestModel
import com.example.logintask.api.models.LoginResponseModel
import com.example.logintask.main.login.models.LoginValidationError
import com.example.logintask.main.login.models.LoginValidationError.*
import com.example.logintask.repositories.Repository
import com.example.logintask.tools.ValidatorHelper.validateEmail
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

class LoginViewModel @Inject constructor(private val repository: Repository) : ViewModel() {

    val email = MutableLiveData<String>()
    val emailValidationError = MutableLiveData<LoginValidationError>()
    val isEmailValid = MutableLiveData<Boolean>()

    val password = MutableLiveData<String>()
    val passwordValidationError = MutableLiveData<LoginValidationError>()
    val isPasswordValid = MutableLiveData<Boolean>()

    val isLoading = MutableLiveData(false)

    val response = MutableLiveData<Response<LoginResponseModel>>()

    fun login() {
        val email = email.value ?: ""
        val password = password.value ?: ""

        isEmailValid.value = when {
            email.isEmpty() -> false.also { emailValidationError.value = NoValue }
            !validateEmail(email) -> false.also { emailValidationError.value = WrongEmailFormat }
            else -> true
        }

        isPasswordValid.value = when {
            password.isEmpty() -> false.also { passwordValidationError.value = NoValue }
            else -> true
        }

        if (isEmailValid.value == false || isPasswordValid.value == false) return

        isLoading.value = true
        viewModelScope.launch {
            response.value = repository.login(LoginRequestModel(email, password))
            isLoading.value = false
        }
    }
}