package com.example.logintask.main.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.example.logintask.databinding.FragmentLoginBinding
import com.example.logintask.di.Injectable
import com.example.logintask.main.login.LoginFragmentDirections.Companion.actionLoginToMain
import javax.inject.Inject

class LoginFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this, viewModelFactory).get(LoginViewModel::class.java)

        val binding = FragmentLoginBinding.inflate(inflater, container, false).apply {
            vm = viewModel
            lifecycleOwner = viewLifecycleOwner

            passwordEditText.setOnEditorActionListener { _, actionId, _ ->
                return@setOnEditorActionListener when (actionId) {
                    EditorInfo.IME_ACTION_SEND -> true.also { viewModel.login() }
                    else -> false
                }
            }

            loginButton.setOnClickListener { viewModel.login() }
        }

        bind()

        return binding.root
    }

    private fun bind() {
        viewModel.response.observe(this, Observer {
            if (it.isSuccessful) {
                view?.findNavController()?.navigate(actionLoginToMain())
            } else {
                toast("${it.code()} ${it.message()}")
            }
        })
    }

    private fun toast(message: String) = Toast.makeText(context, message, LENGTH_LONG).show()
}