package com.example.logintask.tools

import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import com.example.logintask.main.login.models.LoginValidationError
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

@BindingAdapter("visibility")
fun setVisibility(view: View, isVisible: Boolean?) {
    view.visibility = if (isVisible == true) VISIBLE else GONE
}

@BindingAdapter("errorText")
fun setErrorMessage(view: TextInputLayout, errorMessage: LoginValidationError?) {
    errorMessage?.text?.let { view.error = view.context.getText(it) }
}

@BindingAdapter("errorEnabled")
fun setErrorEnable(view: TextInputLayout, isEmailValid: Boolean?) {
    view.isErrorEnabled = isEmailValid?.let { !it } ?: false
}

@InverseBindingAdapter(attribute = "android:text", event = "android:textAttrChanged")
fun getText(view: TextInputEditText): String {
    return view.text.toString()
}