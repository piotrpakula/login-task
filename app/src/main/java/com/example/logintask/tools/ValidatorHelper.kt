package com.example.logintask.tools

object ValidatorHelper {

    private const val emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\\.+[a-z]+"

    fun validateEmail(email: String): Boolean {
        return email.trim().matches(Regex(emailPattern))
    }
}